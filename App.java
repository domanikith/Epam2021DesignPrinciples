package com.myepam.app;
import java.util.*;
import java.util.Scanner;
public class App 
{
    public static void main( String[] args )
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("First Number");
        double num1 = sc.nextDouble();
        System.out.println("Second number :");
        double num2 = sc.nextDouble();
        System.out.println("Select any one{+,-,*,/}");
        char e= sc.next().charAt(0);
        Calculator cal = new Calculator(num1,num2,e);
        cal.eval();
        cal.disp();
        sc.close();
    }
}